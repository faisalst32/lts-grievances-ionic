
export interface UserLocation{
    username: string;
    coordinates: string;
}