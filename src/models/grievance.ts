export interface Grievance{
    Id: number;
    Type: string;
    Text: string;
    ImageUrl: string;
    Date: string;
    DeviceId: string;
    UniqueId: string;
    Status: string;
}