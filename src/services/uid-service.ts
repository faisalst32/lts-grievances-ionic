import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';

import { Injectable } from '@angular/core';


Injectable()
export class UidService{

    public IMEI: string;

    public perm: AndroidPermissions;

    public uid: Uid;

    async getIMEI(){
        const { hasPermission } = await this.perm.checkPermission(
            this.perm.PERMISSION.READ_PHONE_STATE
          );
         
          if (!hasPermission) {
            const result = await this.perm.requestPermission(
              this.perm.PERMISSION.READ_PHONE_STATE
            );
         
            if (!result.hasPermission) {
              throw new Error('Permissions required');
            }
         
            return;
          }
         
           return this.uid.IMEI;
    }

}