import { HttpClient } from '@angular/common/http';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class SharedService {

    public baseUrl: string = 'http://10.1.50.138:97/';
    // public baseUrl: string = 'http://localhost:54131/';
    // public baseUrl: string = 'http://liventus.azurewebsites.net/';
    // public deviceId: string = 'f605fef8-4fd5-6087-8618-030405690928';
    public deviceId: string;
    public playerId: string;

    public role: string;

    constructor(private uid: UniqueDeviceID,
        private androidPermissions: AndroidPermissions,
        private http: HttpClient) { }

    async loadDeviceId() {
        try{
            await this.uid.get().then(data => {
                this.deviceId = data;
            });
        }
        catch(error){
            
        }
        

    }

    getDeviceId() {
        try{
            if (!this.deviceId) {
                this.loadDeviceId();
                return this.deviceId;
            }
            else {
                return this.deviceId;
            }
        }
        catch(error){

        }
    }


    getUserRole(deviceId: string): Observable<string> {
        if (!this.role)
            return this.http.get<string>(this.baseUrl + 'api/user/GetUserRole' + '?deviceId=' + deviceId);
        else
            return Observable.of(this.role);
    }
}