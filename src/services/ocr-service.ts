import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SharedService } from './shared-service';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';


@Injectable()
export class OCRService{
    private baseUrl: string;
    private httpOptions = {

        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }),

    }

    constructor(
        private sharedService: SharedService,
        private http: HttpClient) {
        this.baseUrl = sharedService.baseUrl;
    }

    getOcrText(imageData: string): Observable<string>{
        return this.http.post<string>(this.baseUrl + 'api/ocr/ExtractText', 
            {ImageData: imageData}, this.httpOptions);
    }

    
}