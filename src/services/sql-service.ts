import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Injectable } from "@angular/core";


@Injectable()
export class SqlService{


    
    public db : SQLiteObject;

    constructor(private sqlite: SQLite) {}

    public intializeDatabase() {

        return this.sqlite.create({name: 'data.db', location: 'default'})
            .then(db => {
                this.db = db;
                return this.db.executeSql('CREATE TABLE IF NOT EXISTS USER (Name text, UID text)', [])
                    .then(() => {
                        return this.db.executeSql('CREATE TABLE IF NOT EXISTS LOCATIONS (UserName text, COORDINATES text, Timestamp text )', [])
                    },
                    error => console.info(error));
            },
            error => console.info(error));

    }
}