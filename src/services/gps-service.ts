import { SqlService } from './sql-service';
import { UserLocation } from './../models/location';
import { Injectable } from "@angular/core";
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { SharedService } from './shared-service';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
declare var window: any;


@Injectable()
export class GpsService {


    private deviceLocation: Geoposition;
    private locationLastUpdated: Date;
    

    constructor(private sqlService: SqlService,
                private geolocation: Geolocation) { }

    saveLocation(userLocation: UserLocation) {


        // if (!this.deviceLocation || (this.locationLastUpdated.valueOf() - new Date().valueOf() > 300000)){
        // if (true){
        return this.geolocation.getCurrentPosition()
            .then(data => {

                console.log(window);

                this.deviceLocation = data;
                this.locationLastUpdated = new Date();
                console.log(this.deviceLocation);
                console.log(this.locationLastUpdated);
                return this.sqlService.db.executeSql("INSERT INTO LOCATIONS (UserName, COORDINATES, Timestamp) values (?, ?, ?)", 
                                                        [userLocation.username, 'lat: ' + this.deviceLocation.coords.latitude + 'lng: ' + this.deviceLocation.coords.longitude, new Date().toString()])
                                                         .then(data => console.log(data));
            });

    }

    listLocations() {
        return this.sqlService.db.executeSql("SELECT UserName, COORDINATES from LOCATIONS", [])
            .then(data => {
                console.log(data);
                let locations: UserLocation[] = [];
                console.log(data.rows.item(1));
                for (var i = 0; i < data.rows.length; i++) {
                    locations.push({ username: data.rows.item(i).UserName, coordinates: data.rows.item(i).COORDINATES })
                }
                console.log(locations);
                return locations;
            },
                error => console.log(error));
    }

    startTrackingLocation(isActivate, playerId){
 
        let backgroundGL = window.BackgroundGeolocation;

        const config = {
            desiredAccuracy: 20,
            stationaryRadius: 0,
            distanceFilter: 0,
            // debug: true, //  enable this hear sounds for background-geolocation life-cycle.
            stopOnTerminate: false,
            interval: 300000,
            notificationsEnabled: false,
            maxLocations: 30,
            startForeground: false,
            fastestInterval: 300000,
            // url: 'http://localhost:54131/api/Places/CheckLocation',
            // syncUrl: 'http://localhost:54131/api/Places/CheckLocation',
            url: 'http://10.1.50.138:97/api/Places/CheckLocation',
            // syncUrl: 'http://10.1.50.138:97/api/Places/CheckLocation',
            postTemplate: {
                Latitude: '@latitude',
                Longitude: '@longitude',
                foo: playerId
              }
        };
        backgroundGL.on('location', function(location) {
            console.log('Location Logged');
          });

        backgroundGL.configure(config);
        backgroundGL.checkStatus(function(status) {
            console.log('[INFO] BackgroundGeolocation service is running', status.isRunning);
            console.log('[INFO] BackgroundGeolocation services enabled', status.locationServicesEnabled);
            console.log('[INFO] BackgroundGeolocation auth status: ' + status.authorization);
        
            // you don't need to check status before start (this is just the example)
            if (!status.isRunning) {
                backgroundGL.start(); //triggers start on start event
            }
          });
       
    }

    stopTrackingLocation(){
        let backgroundGL = window.BackgroundGeolocation;
        backgroundGL.stop();
    }

    showTrackingInformation(){
        
        let backgroundGL = window.BackgroundGeolocation;
        backgroundGL.getLocations((locations) => {
            console.log(locations);
        },
        error => console.log(error));
    }




}