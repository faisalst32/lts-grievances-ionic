import { SharedService } from './shared-service';
import { Observable } from 'rxjs/Observable';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable, OnInit } from "@angular/core";
import { Grievance } from '../models/grievance';
import 'rxjs/add/observable/of';


@Injectable()
export class GrievanceService {

    private grievances: Grievance[] = [];

    private baseUrl: string;
    private deviceId: string;

    private httpOptions = {

        headers: new HttpHeaders({
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }),

    }

    constructor(private _http: HttpClient,
        private sharedService: SharedService) {
        this.baseUrl = this.sharedService.baseUrl;

    }

    getGrievances(forceLoad: boolean = false): Observable<Grievance[]> {

        let deviceId: string = this.sharedService.getDeviceId();

        if (forceLoad || this.grievances.length == 0)
            return this._http.get<Grievance[]>(this.sharedService.baseUrl + 'api/grievance/GetGrievances?' + "deviceId=" + deviceId);
        else (!forceLoad && this.grievances.length > 0)
        return Observable.of(this.grievances);
    }



    saveGrievance(grievance: Grievance) {

        return this._http.post(this.sharedService.baseUrl + 'api/grievance/AddGrievance', grievance, this.httpOptions);
    }

    storeGrievances(grievances: Grievance[]) {

        this.grievances = grievances;
    }

    deleteGrievance(uniqueId: string) {
        return this._http.post(this.sharedService.baseUrl + 'api/grievance/DeleteGrievance', { UniqueId: uniqueId }, this.httpOptions);
    }


    escalateGrievance(uniqueId: string) {
        
        return this._http.post(this.baseUrl + 'api/grievance/EscalateGrievance', { UniqueId: uniqueId }, this.httpOptions);

    }


    





}