import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
declare var window: any;

@Injectable()
export class PaymentsService {

    constructor(private http: HttpClient){}

    createNewPayment(description, amount, email, contact, name, orderId) {
        let razorpay = window.RazorpayCheckout;

        let options = {
            description: description,
            image: 'https://i.imgur.com/3g7nmJC.png',
            currency: 'INR',
            key: 'rzp_test_FaJcEmqcbrdHO8',
            order_id: orderId,
            amount: amount,
            name: 'LTS Demo Merchant',
            prefill: {
                email: email,
                contact: contact,
                name: name
            },
            theme: {
                color: '#F37254'
            }
        }

        var successCallback = function(data) {
            console.log(data);
            var orderId = data.razorpay_order_id
            var signature = data.razorpay_signature

          }
          
          var cancelCallback = function(error) {
            console.log(error);
          }

          razorpay.on('payment.success', successCallback)
          razorpay.on('payment.cancel', cancelCallback)
          razorpay.open(options)
    }

    retrieveOrderId(){
        return this.http.get<string>("http://10.1.50.138:97/api/Payments/RetrieveOrderId");
    }
}