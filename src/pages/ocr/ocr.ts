import { Clipboard } from '@ionic-native/clipboard';
import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController } from 'ionic-angular';
import { Camera, CameraOptions, DestinationType, PictureSourceType } from '@ionic-native/camera';
import * as Tesseract from 'tesseract.js'
import { NgProgress } from 'ngx-progressbar';
import * as Cloudmersive from 'cloudmersive-ocr-api-client';
import { OCRService } from '../../services/ocr-service';

@Component({
  selector: 'page-ocr',
  templateUrl: 'ocr.html',
})
export class OcrPage {

  image: string;
  imageText: string;
  constructor(public navCtrl: NavController,
              private camera: Camera,
              private loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private ocrService: OCRService,
              private clipboard: Clipboard) {}

  onTakePicture(){
    this.imageText = null;
    let options: CameraOptions = {
      destinationType: DestinationType.DATA_URL,
      sourceType: PictureSourceType.CAMERA,
      targetWidth: 2000
    }

    this.camera.getPicture(options).then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    });
  }
 
  onSelectImage(){
    this.imageText = null;
    let options: CameraOptions = {
      destinationType: DestinationType.DATA_URL,
      sourceType: PictureSourceType.PHOTOLIBRARY
    }

    this.camera.getPicture(options).then(imageData => {
      this.image = `data:image/jpeg;base64,${imageData}`;
    });
  }

  onRecognizeImage(){
    let loader = this.loadingCtrl.create({
      content: 'Recognizing Text...',
      spinner: 'dots'
    });
    loader.present();
    // Tesseract.recognize(this.image)
    // .progress(message => {
       
      
    // })
    // .catch(err => {
    //   console.error(err);
    //   this.showToast(err.toString());
    //   loader.dismiss();
    // })
    // .then(result => {
    //   this.imageText = result.text;
    // })
    // .finally(resultOrError => {
    //   loader.dismiss();
    // });

    // let Cloud = require('cloudmersive-ocr-api-client');
    // let defaultClient = Cloud.ApiClient.instance;
    console.log(this.image);

    this.ocrService.getOcrText(this.image).subscribe(data => {
      loader.dismiss();
      this.imageText = data
    },
    error => {
      loader.dismiss();
      console.log(error);
      this.showToast('Failed to process image. Please try again.')
    });
    
  }

  onCopyToClipboard(){
    this.clipboard.copy(this.imageText);
    let toast = this.toastCtrl.create({
      message: "Text copied to clipboard",
      duration: 1000
    });
    toast.present();
  }

  showToast(message: string){
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      closeButtonText: 'Ok',
      showCloseButton: true
    });
    toast.present();
  }




}
