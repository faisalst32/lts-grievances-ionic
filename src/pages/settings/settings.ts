import { OnInit } from '@angular/core';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { SharedService } from '../../services/shared-service';

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage implements OnInit {

  public server: string;

  constructor(public navCtrl: NavController, 
    public sharedService: SharedService) {
  }

  saveSettings(){
    this.sharedService.baseUrl = this.server;
    this.navCtrl.popToRoot();
  }

  ngOnInit(){
    this.server = this.sharedService.baseUrl;
  }

  

}
