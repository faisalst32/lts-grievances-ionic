import { AddGrievancePage } from './../add-grievance/add-grievance';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@Component({
  selector: 'page-grievance-categories',
  templateUrl: 'grievance-categories.html',
})
export class GrievanceCategoriesPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  addGrievance(type: string){
    this.navCtrl.push(AddGrievancePage, type);
  }

}
