import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { Calendar } from '@ionic-native/calendar';
import { AlertController } from 'ionic-angular';
import { File } from '@ionic-native/file';

@Component({
  selector: 'page-calendar',
  templateUrl: 'calendar.html',
})
export class CalendarPage {

  constructor(private calendar: Calendar,
              private http: HttpClient,
              private alertCtrl: AlertController,
              private file: File) {
  }

  onScheduleEvent(){
    this.calendar.createEvent('LTS Meeting', 'Board Room', 'Be there', new Date("2019-12-27T18:00:00"), new Date("2019-12-27T19:00:00"))
              .then(data => {
                  
              },
                    error => console.log(error));
  }

  onEncryptFile(){

  }

  onDecryptFile(){


  }

  onReadFile(){
    const fileLocation = this.file.externalApplicationStorageDirectory;
    console.log(fileLocation);

    this.file.readAsDataURL(fileLocation, 'encrypt.json').then(data => {
      console.log(data);
      const alert = this.alertCtrl.create({
        title: 'File Contents',
        message: JSON.stringify(data),
        buttons: [
          {
            text: 'Ok',
            role: 'cancel'
          }
        ]
      });

      alert.present();
    });
  }
  


}
