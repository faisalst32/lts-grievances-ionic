import { SharedService } from './../../services/shared-service';
import { OnInit } from '@angular/core';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { GrievanceService } from './../../services/grievance-service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { Guid } from 'guid-typescript';
import * as moment from 'moment';
import { WebView } from '@ionic-native/ionic-webview/ngx'
import { File } from '@ionic-native/file';



@Component({
  selector: 'page-add-grievance',
  templateUrl: 'add-grievance.html',
})
export class AddGrievancePage implements OnInit {
  public imageUri: string;
  grievanceText: any;
  grievanceType: any;
  date: string = moment().format('DD/MM/YYYY HH:mm');
  deviceId: string;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public camera: Camera,
    public toastCtrl: ToastController,
    public webview: WebView,
    public file: File,
    private grievanceService: GrievanceService,
    private loadingCtrl: LoadingController,
    private uid: UniqueDeviceID,
    private sharedService: SharedService) {
  }

  takePicture() {
    let options: CameraOptions = {
      destinationType: this.camera.DestinationType.FILE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      saveToPhotoAlbum: false,
      targetWidth: 800,
      quality: 100,
      correctOrientation: true
    }

    this.camera.getPicture(options).then((imageData) => {

      let filename = imageData.substring(imageData.lastIndexOf('/') + 1);
      let path = imageData.substring(0, imageData.lastIndexOf('/') + 1);
      this.file.readAsDataURL(path, filename).then(res => this.imageUri = res);
    });
  }

  getId() {
    return Guid.create().toString();
  }


  submitGrivance() {
    if (this.grievanceType && this.grievanceText) {

      const loader = this.loadingCtrl.create({
        content: 'Saving Grievance...',
        spinner: 'dots'
      });
      loader.present();
     
      this.grievanceService.saveGrievance({


        Type: this.grievanceType,
        Text: this.grievanceText,
        Date: this.date,
        ImageUrl: this.imageUri,
        DeviceId: this.deviceId,
        Id: 0,
        UniqueId: this.getId(),
        Status: "New"
      })
        .subscribe((data) => {
          console.log(data);
          loader.dismiss();
          let toast = this.toastCtrl.create({
            message: 'Your grievance has been successfully registered.',
            duration: 5000,
            showCloseButton: true
          });
          toast.present().then(() => {
            this.navCtrl.popToRoot();
          });
        },
          error => {
            console.log(error);
            loader.dismiss();
            let toast = this.toastCtrl.create({
              message: JSON.stringify(error.message),
              duration: 5000,
              showCloseButton: true
            })
            toast.present();
          });
    }

    else {
      let toast = this.toastCtrl.create({
        message: "'Details' field cannot be empty",
        duration: 5000,
        showCloseButton: true
      })
      toast.present()
    }
  }

  ionViewWillEnter() {
    this.grievanceType = this.navParams.data;
    
  }

  ngOnInit(){ 
    this.deviceId = this.sharedService.getDeviceId(); 
  }






}
