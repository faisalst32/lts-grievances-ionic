import { Component } from '@angular/core';
import { PaymentsService } from '../../services/payments-service';
import { InAppBrowser } from '@ionic-native/in-app-browser';

@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {
  

  constructor(private paymentsService: PaymentsService,
              private inAppBrowser: InAppBrowser){}

  onStartPayment(){
    this.paymentsService.retrieveOrderId().subscribe(orderId => {
      debugger;
      console.log(orderId);
      this.paymentsService.createNewPayment("test", "50000", "test@test.com", "1234567890", "Faisal", orderId);
    });
    
  }

  onViewDetails(){
    let page = this.inAppBrowser.create("https://razorpay.com/pricing/", "_self", 'location=no,zoom=no');
  }
}
