import { UserLocation } from './../../models/location';
import { Component } from '@angular/core';
import { GpsService } from '../../services/gps-service';
import { HttpClient } from '@angular/common/http';
import { Geolocation } from '@ionic-native/geolocation';



@Component({
  selector: 'page-gps-tracking',
  templateUrl: 'gps-tracking.html',

})
export class GpsTrackingPage {

  constructor(private gpsService: GpsService,
              private http: HttpClient,
              private gps: Geolocation){}

  locations: UserLocation[];

  private location: UserLocation;

  onSaveCoordinates(userLocation: UserLocation){
    // TODO: Remove Hardcoded Values
    this.gpsService.saveLocation({username: 'Faisal', coordinates: '300.11541 562.32265'})
      .then(() => console.log('Location Saved'),
            error => console.log('Error: ' + error))
  }

  onListCoordinates(){
    this.gpsService.listLocations()
      .then(locations => {
        console.log(locations);
        if(locations) {
          this.locations = <UserLocation[]>locations;
        }
      },
      error => console.log(error))
  }

  onShowTrackingInformation(){
    this.gpsService.showTrackingInformation();
  }

  onPostLocation(){
    let data;
    this.gps.getCurrentPosition({
      enableHighAccuracy: true
      
    }).then((position) =>{
      console.log(data);
      data = [{
        Latitude: position.coords.latitude.toString(),
        Longitude: position.coords.longitude.toString(),
        TimeStamp: '2019-01-01',
        PlayerId: '7a2d17cb-fef2-4687-b852-efc8fdbb977e'
      }];
      this.http.post('http://10.1.50.138:97/api/Places/CheckLocation', data).subscribe(data => console.log(data));
    })
  }

  onStopTracking(){
    this.gpsService.stopTrackingLocation
  }

  onStartTrackingLocation(){
    this.gpsService.startTrackingLocation("true", "7a2d17cb-fef2-4687-b852-efc8fdbb977e");
  }

}
