import { SharedService } from './../../services/shared-service';
import { AddGrievancePage } from './../add-grievance/add-grievance';
import { GrievancePage } from './../grievance/grievance';
import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, LoadingController, AlertController, ToastController } from 'ionic-angular';
import { Grievance } from '../../models/grievance';
import { GrievanceService } from './../../services/grievance-service';
import { Vibration } from '@ionic-native/vibration';



@Component({
  selector: 'page-my-grievances',
  templateUrl: 'my-grievances.html',

})
export class MyGrievancesPage {

  myGrievances: Grievance[] = [];

  public addGrievance = AddGrievancePage;

  @ViewChild('fab') fab: any;



  tappedItem: number;

  showControls: boolean = false;


  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private _grievanceService: GrievanceService,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private sharedService: SharedService,
    private vibration: Vibration) {
  }


  ionViewWillEnter() {
    this.sharedService.loadDeviceId().then(() => {
      this.getGrievances();
    });
  }

  ionViewWillLeave() {
    this.fab.close();
  }




  getGrievanceColor(grievance: Grievance) {
    switch (grievance.Type.toLowerCase()) {
      case 'issue':
        return 'primary';
      case 'suggestion':
        return 'secondary';
      case 'complaint':
        return 'danger';
      default:
        return 'orange';
    }
  }

  goToGrievance(grievance: Grievance) {
    this.navCtrl.push(GrievancePage, grievance);
  }

  addGrievancePage(type: string) {
    this.navCtrl.push(AddGrievancePage, type);
  }

  toggleControls(index: number) {
    if (this.tappedItem == index)
      this.tappedItem = -1;
    else
      this.tappedItem = index;

  }

  checkExpanded(index: number) {

    return index == this.tappedItem
  }



  getGrievances(refresher = null, forceRefresh: boolean = false) {
    const loader = this.loadingCtrl.create({
      content: 'Loading',
      spinner: 'dots'
    });
    loader.present();
    
    this._grievanceService.getGrievances(forceRefresh).subscribe(data => {
      console.log(data);
      this.myGrievances = data;
      this._grievanceService.storeGrievances(data);
      loader.dismiss();
      if (refresher){
        this.vibration.vibrate(100);
        refresher.complete();
      }
    },
      (error) => {
        loader.dismiss();
        this.createToast('Failed to load data.');
        if (refresher){
          this.vibration.vibrate(100);
          refresher.complete();
        }
      });

    return this.myGrievances;
  }

  deleteGrievance(uniqueId: string) {
    let toast = this.toastCtrl.create({
      message: 'Grievance deleted successfully',
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    let alert = this.alertCtrl.create({
      title: 'Delete Grievance',
      message: 'Are you sure you want to delete this grievance?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this._grievanceService.deleteGrievance(uniqueId).subscribe(() => {
            toast.present();
            this.getGrievances(null, true);
          },
            error => {
              toast.dismiss();
            });
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    });
    alert.present();
  }

  escalateGrievance(){
    let toast = this.toastCtrl.create({
      message: 'Your Grievance has been escalated. The relevant authorities will get back to you soon.',
      duration: 5000,
      showCloseButton: true
    });
    toast.present(); 
  }

  createToast(message: string) {
    const toast = this.toastCtrl.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Ok'
    });
    toast.present();
  }



}
