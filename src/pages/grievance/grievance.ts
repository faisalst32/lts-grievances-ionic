import { SharedService } from './../../services/shared-service';
import { GrievanceService } from './../../services/grievance-service';
import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { Grievance } from '../../models/grievance';

@Component({
  selector: 'page-grievance',
  templateUrl: 'grievance.html',
})
export class GrievancePage implements OnInit {
  public grievance: Grievance;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public grivanceService: GrievanceService,
    private sharedService: SharedService) {
  }

  ngOnInit() {
    this.grievance = this.navParams.data;
  }

  deleteGrievance() {
    let toast = this.toastCtrl.create({
      message: 'Grievance deleted successfully',
      duration: 5000,
      showCloseButton: true
    })
    let alert = this.alertCtrl.create({
      title: 'Delete Grievance',
      message: 'Are you sure you want to delete this grievance?',
      buttons: [{
        text: 'Yes',
        handler: () => {
          this.grivanceService.deleteGrievance(this.grievance.UniqueId).subscribe(() => {
            toast.present();
            this.grivanceService.getGrievances(true).subscribe(() => {
              this.navCtrl.popToRoot();
            })
          });
        }
      },
      {
        text: 'Cancel',
        role: 'cancel'
      }]
    });
    alert.present();


  }

  escalateGrievance(){
    let toast = this.toastCtrl.create({
      message: 'Your Grievance has been escalated. The relevant authorities will get back to you soon.',
      duration: 5000,
      showCloseButton: true
    });
    toast.present(); 
  }

  getImageUrl(){
    if(this.grievance.ImageUrl)
    return this.sharedService.baseUrl + this.grievance.ImageUrl.slice(2);
  }



}
