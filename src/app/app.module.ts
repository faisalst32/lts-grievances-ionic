import { GpsTrackingPage } from './../pages/gps-tracking/gps-tracking';
import { CalendarPage } from './../pages/calendar/calendar';
import { AdminPage } from './../pages/admin/admin';
import { SettingsPage } from './../pages/settings/settings';
import { GrievanceControlsComponent } from './../components/grievance-controls.component';
import { GrievanceCategoriesPage } from './../pages/grievance-categories/grievance-categories';
import { GrievancePage } from './../pages/grievance/grievance';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { MyApp } from './app.component';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { AddGrievancePage } from '../pages/add-grievance/add-grievance';
import { MyGrievancesPage } from '../pages/my-grievances/my-grievances';
import { Camera } from '@ionic-native/camera';
import { IonicStorageModule } from '@ionic/storage';
import { File } from '@ionic-native/file';
import { HttpClientModule } from '@angular/common/http';
import { UidService } from '../services/uid-service';
import { Uid } from '@ionic-native/uid';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { GrievanceService } from '../services/grievance-service';
import { SharedService } from '../services/shared-service';
import { NgProgressModule } from 'ngx-progressbar';
import { OcrPage } from '../pages/ocr/ocr';
import { OCRService } from '../services/ocr-service';
import { Clipboard } from '@ionic-native/clipboard';
import { Calendar } from '@ionic-native/calendar';
import { Vibration } from '@ionic-native/vibration';
import { SqlService } from '../services/sql-service';
import { SQLite } from '@ionic-native/sqlite';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { GpsService } from './../services/gps-service';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { Device } from '@ionic-native/device';
import { PaymentsPage } from '../pages/payments/payments';
import { PaymentsService } from '../services/payments-service';
import { InAppBrowser} from '@ionic-native/in-app-browser'
import { AudioLibraryPage } from '../pages/audio-library/audio-library';




@NgModule({ 
  declarations: [ 
    MyApp, 
    AddGrievancePage,
    MyGrievancesPage,
    GrievancePage,
    GrievanceCategoriesPage,
    SettingsPage,
    GrievanceControlsComponent,
    AdminPage,
    OcrPage,
    CalendarPage,
    GpsTrackingPage,
    PaymentsPage,
    AudioLibraryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicStorageModule.forRoot(),
    NgProgressModule,
    IonicModule.forRoot(MyApp)
  ], 
  exports: [
    GrievanceControlsComponent
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AddGrievancePage, 
    MyGrievancesPage,
    GrievancePage,
    GrievanceCategoriesPage,
    SettingsPage,
    AdminPage,
    OcrPage,
    CalendarPage,
    GpsTrackingPage,
    PaymentsPage,
    AudioLibraryPage
  ],
  providers: [
    StatusBar,
    SplashScreen, 
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UniqueDeviceID,
    Camera, 
    UidService,
    Uid,
    AndroidPermissions,
    WebView,     
    GrievanceService,
    SharedService,
    OCRService,
    Clipboard,
    Calendar,
    Vibration,
    File,
    SqlService,
    SQLite,
    GpsService,
    Geolocation,
    BackgroundGeolocation,
    LocalNotifications,
    Device,
    PaymentsService,
    InAppBrowser 
  ]
})
export class AppModule {}
