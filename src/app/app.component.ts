import { SqlService } from './../services/sql-service';
import { CalendarPage } from './../pages/calendar/calendar';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import { SettingsPage } from './../pages/settings/settings';
import { SharedService } from './../services/shared-service';
import { GrievanceCategoriesPage } from './../pages/grievance-categories/grievance-categories';
import { Component, ViewChild, OnInit } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { MyGrievancesPage } from '../pages/my-grievances/my-grievances';
import { AddGrievancePage } from '../pages/add-grievance/add-grievance';
import { UniqueDeviceID } from '@ionic-native/unique-device-id';
import { OcrPage } from '../pages/ocr/ocr';
import { GpsTrackingPage } from '../pages/gps-tracking/gps-tracking';
import { GpsService } from '../services/gps-service';
import { Device } from '@ionic-native/device';
import { PaymentsPage } from '../pages/payments/payments';
@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit {
  @ViewChild(Nav) nav: Nav;
  role: string;

  constructor(platform: Platform,
    statusBar: StatusBar,
    private splashScreen: SplashScreen,
    private uid: UniqueDeviceID,
    private sharedService: SharedService,
    private androidPermissions: AndroidPermissions,
    private sqlService: SqlService,
    private gpsService: GpsService) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      statusBar.styleDefault();
      splashScreen.hide();

      // OneSignal Registration


      try {
        var notificationOpenedCallback = function (jsonData) {
          console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        window["plugins"].OneSignal
          .startInit("ba527689-abd6-402a-aafe-c0f5d78e0a4d", "18011546812")
          .handleNotificationOpened(notificationOpenedCallback)
          .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.Notification)
          .endInit();
        let playerId;
        window["plugins"].OneSignal
          .getPermissionSubscriptionState((data) => {
            this.sharedService.playerId = data.subscriptionStatus.userId;
            playerId = data.subscriptionStatus.userId;

            // Start Tracking Location only if Android Version is Oreo and Below (Bug with Background Location Tracker in Android Pie)

            // if (Number(this.device.version.split('.')[0]) < 9)
              this.gpsService.startTrackingLocation("true", playerId);
          })

        // Sql Database Init

        this.sqlService.intializeDatabase()
          .then(() => console.log('Database initialized'));


      }
      catch (error) {
        console.log(error);
      }


    });
  }

  public rootPage = MyGrievancesPage;
  public myGrievancesPage = MyGrievancesPage;
  public addGrievancePage = AddGrievancePage;



  ionViewDidLoad() {

  }

  goToMyGrievances() {
    this.nav.popToRoot();
  }

  goToAddGrievance() {
    this.nav.push(GrievanceCategoriesPage);
  }

  goToSettings() {
    this.nav.push(SettingsPage);
  }

  goToCalendar() {
    this.nav.push(CalendarPage);
  }

  goToGps() {
    this.nav.push(GpsTrackingPage);
  }

  goToPayments() {
    this.nav.push(PaymentsPage);
  }

  async getPermissions() {
    const { hasPermission } = await this.androidPermissions.checkPermission(
      this.androidPermissions.PERMISSION.READ_PHONE_STATE
    );

    if (!hasPermission) {
      const result = await this.androidPermissions.requestPermission(
        this.androidPermissions.PERMISSION.READ_PHONE_STATE
      );

      if (!result.hasPermission) {
        throw new Error('Permissions required');
      }

      // ok, a user gave us permission, we can get him identifiers after restart app
      return;
    }
    this.uid.get().then(data => {
      this.sharedService.deviceId = data;
      this.splashScreen.hide();
    })

  }

  goToOCRDemo() {
    this.nav.push(OcrPage);
  }



  ngOnInit() {

  }
}

