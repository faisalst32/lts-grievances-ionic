import { Component, Input, OnInit } from "@angular/core";

@Component({
    selector: 'app-controls',
    template: `
  <ion-row>
    <ion-col text-center><button ion-button clear color="light" icon-left><ion-icon name="trash"></ion-icon>Delete</button></ion-col>
    <ion-col text-center><button ion-button clear color="orange" icon-left><ion-icon name="md-megaphone"></ion-icon>Escalate</button></ion-col>
  </ion-row>
    `

})
export class GrievanceControlsComponent{
    
}